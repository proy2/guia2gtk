import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class VentanaPrincipal(Gtk.Window):
    def __init__(self):
        super().__init__()

        self.set_title("Analisis Medico")
        self.set_default_size(600,600)
        self.connect("destroy", Gtk.main_quit)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.crear_toolbar()
        self.crear_textview()


        # hacer la toolbar con botones
        # hacer filechooser
        # hacer analisis a partir de los datos
        # hacer que el textview muestre el analisis
    
    def crear_toolbar(self):
        toolbar =Gtk.Toolbar()
        toolbar.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.grid.attach(toolbar, 0, 0, 3, 1)

        boton_abrir_archivo = Gtk.ToolButton(label="Abrir")
        boton_abrir_archivo.connect("clicked", self.ventana_filechooser_abrir)
        toolbar.insert(boton_abrir_archivo, 0)

        boton_guardar = Gtk.ToolButton(label="Guardar")
        boton_abrir_archivo.connect("clicked", self.ventana_filechooser_guardar)
        toolbar.insert(boton_guardar, 1)

    
    def crear_textview(self):
        scrolled = Gtk.ScrolledWindow()
        scrolled.set_hexpand(True)
        scrolled.set_vexpand(True)
        self.grid.attach(scrolled, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.buffer = self.textview.get_buffer()

        scrolled.add(self.textview)

    

    def ventana_filechooser_abrir(self, widget=None):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()


    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python files")
        filter_py.add_mime_type("text/x-python")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)


    def ventana_filechooser_guardar(self, widget=None):
        pass


    def analisis(self, data):
        pass
        


win = VentanaPrincipal()
win.show_all()
Gtk.main()