import random
import json


nombres_hombre = ["Juan", "Pedro", "Matias", "Felipe"]
nombres_mujer = ["Maria", "Ana", "Camila", "Sofia"]
apellidos = ["Soto", "Martinez", "Muñoz", "Rodriguez", "Valdivia", "Torres"]

def crear_ficha(data):
    with open(f'{data["Nombre"]}_{data["Apellido"]}.json', "w") as archivo:
        archivo.write(json.dumps(data))




for x in range(3):
    sexo = random.choice(("M", "F"))
    if sexo == "M":
        nombre = random.choice(nombres_hombre)
    else:
        nombre = random.choice(nombres_mujer)
    apellido = random.choice(apellidos)
    peso = random.randint(50, 120)
    altura = random.randint(150, 190)
    edad = random.randint(18, 40)

    data = {"Nombre" : f"{nombre}",
            "Apellido" : f"{apellido}",
            "Sexo" : f"{sexo}",
            "Peso" : f"{peso}",
            "Altura" : f"{altura}",
            "Edad" : f"{edad}"
            }
        
    crear_ficha(data)